package client;

import client.config.CredentialsConfig;
import client.model.Record;
import client.repository.RecordRepository;
import client.services.json.builder.JsonRequestBuilder;
import client.services.json.builder.JsonRequestBuilderIml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ClientApplication implements CommandLineRunner {

    private final RecordRepository recordRepository;
    private final CredentialsConfig credentialsConfig;

    @Autowired
    public ClientApplication(RecordRepository recordRepository, CredentialsConfig credentialsConfig) {
        this.recordRepository = recordRepository;
        this.credentialsConfig = credentialsConfig;
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.basicAuthorization(credentialsConfig.getUsername(), credentialsConfig.getPassword()).build();
    }

    @Bean
    public JsonRequestBuilder jsonRequestBuilder() {
        return new JsonRequestBuilderIml();
    }

    public static void main(String[] args) {
        SpringApplication.run(ClientApplication.class, args);
    }

    @Override
    public void run(String... strings) {
        recordRepository.save(new Record("one", "ONE"));
        recordRepository.save(new Record("two", "TWO"));
    }
}
