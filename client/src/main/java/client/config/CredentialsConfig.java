package client.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.io.Serializable;

@Getter
@Setter
@ToString
@Configuration
@ConfigurationProperties("atlassian.credential")
public class CredentialsConfig implements Serializable {

    String username;

    String password;
}
