package client.controller;

import client.model.Project;
import client.model.Record;
import client.services.AtlassianService;
import client.services.json.builder.JsonRequestBuilderIml;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@RestController
public class AtlassianController {

    private static final Logger log = LoggerFactory.getLogger(AtlassianController.class);

    private final RestTemplate restTemplate;
    private final JsonRequestBuilderIml jsonRequestBuilder;
    private final AtlassianService atlassianService;

    @Autowired
    public AtlassianController(RestTemplate restTemplate, JsonRequestBuilderIml jsonRequestBuilder, AtlassianService atlassianService) {
        this.restTemplate = restTemplate;
        this.jsonRequestBuilder = jsonRequestBuilder;
        this.atlassianService = atlassianService;
    }

    @RequestMapping("/db")
    public ResponseEntity<Object> getRecords() {
        return new ResponseEntity<>(atlassianService.getRecords(), HttpStatus.OK);
    }

    @RequestMapping("/atlassian2")
    public String getAtlassian2(@RequestBody Project project) throws IOException {
        atlassianService.createPoject();
        atlassianService.getProject();
        String name = project.getName();
        String key = project.getKey();
        atlassianService.addRecord(new Record(name, key));
        if (project.getBitbucket()) {
            String url = "https://api.bitbucket.org/2.0/repositories/deflorator1980/" + name;
            String requestJson = jsonRequestBuilder.getBitbucketJson();

            writeAtlassian(url, requestJson);
        }

        if (project.getJira()) {
            String url = "https://webdefl.atlassian.net/rest/api/2/project";
            String requestJson = jsonRequestBuilder.getJiraJson(key, name);

            writeAtlassian(url, requestJson);
        }

        if (project.getConfluence()) {
            String url = "https://webdefl.atlassian.net/wiki/rest/api/space/_private";
            String requestJson = jsonRequestBuilder.getConfluenceJson(key, name);
            writeAtlassian(url, requestJson);
        }

        return project.getName() + " " + project.getBitbucket() + " " + project.getJira() +
                " " + project.getConfluence();
    }

    private void writeAtlassian(String url, String requestJson) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(requestJson, headers);
        try {
            String answer = restTemplate.postForObject(url, entity, String.class);
            log.info(answer);
        } catch (HttpClientErrorException e) {
            log.error(e.getResponseBodyAsString());
            log.error(e.getMessage());
        }
    }

}
