package client.services.json.builder;

import java.io.IOException;

public interface JsonRequestBuilder {

    String getBitbucketJson() throws IOException;
    String getJiraJson(String key, String name) throws IOException;
    String getConfluenceJson(String key, String name) throws IOException;
}
