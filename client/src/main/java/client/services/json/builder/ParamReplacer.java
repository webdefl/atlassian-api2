package client.services.json.builder;

import org.apache.commons.collections4.MapUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Map;

@Service
public class ParamReplacer {

    public String replaceParams(final String input, Map<String, String> params) {
        if (StringUtils.isEmpty(input) || MapUtils.isEmpty(params)) {
            return input;
        }

        String result = input;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            result = StringUtils.replace(result, entry.getKey(), entry.getValue());
        }
        return result;
    }
}
