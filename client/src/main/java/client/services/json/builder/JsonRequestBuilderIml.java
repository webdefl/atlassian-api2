package client.services.json.builder;

import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class JsonRequestBuilderIml implements JsonRequestBuilder {

    @Override
    public String getBitbucketJson() throws IOException {
        return "{\"scm\": \"git\", \"is_private\": \"true\", \"fork_policy\": \"no_public_forks\"}";
    }

    @Override
    public String getJiraJson(String key, String name) {
        return "{\n" +
                "  \"key\":\"" + key + "\",\n" +
                "  \"name\":\"" + name + "\",\n" +
                "  \"projectTypeKey\": \"business\",\n" +
                "  \"projectTemplateKey\": \"com.atlassian.jira-core-project-templates:jira-core-project-management\",\n" +
                "  \"description\": \"Example Project description\",\n" +
                "  \"lead\": \"admin\",\n" +
                "  \"url\": \"http://atlassian.com\",\n" +
                "  \"assigneeType\": \"PROJECT_LEAD\"\n" +
                "}";
    }

    @Override
    public String getConfluenceJson(String key, String name) throws IOException {
        return "{\n" +
                "    \"description\": {\n" +
                "       \"plain\": {\n" +
                "        \"representation\": \"string\",\n" +
                "        \"value\": \"string\"\n" +
                "      }\n" +
                "    },\n" +
                "  \"key\":\"" + key + "\",\n" +
                "  \"name\":\"" + name + "\"\n" +
                "}";
    }
}
