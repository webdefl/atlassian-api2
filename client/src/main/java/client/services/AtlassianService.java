package client.services;

import client.model.Project;
import client.model.Record;
import client.repository.RecordRepository;
import org.springframework.stereotype.Service;

@Service
public class AtlassianService {

    private final RecordRepository recordRepository;

    public AtlassianService(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    public Iterable<Record> getRecords() {
        return recordRepository.findAll();
    }

    public void createPoject() {

    }

    public Project getProject() {
        return null;
    }

    public void addRecord(Record record) {
        recordRepository.save(record);
    }
}
