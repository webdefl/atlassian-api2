package client.model;

import lombok.Data;

@Data
public class Project {
    private String name;
    private String key;
    private Boolean bitbucket;
    private Boolean jira;
    private Boolean confluence;
}
